package main

import "fmt"

func fizzbuzz(limit int) (result string) {
	for i := 1; i <= limit; i++ {
		switch {
		case i%3 == 0 && i%5 == 0:
			result = result + "FizzBuzz"
		case i%3 == 0:
			result = result + "Fizz"
		case i%5 == 0:
			result = result + "Buzz"
		default:
			result = result + fmt.Sprint(i)
		}
		if i != limit {
			result = result + " "
		}
	}
	return
}

func main() {
	fiz100 := fizzbuzz(100)
	fmt.Print(fiz100)
}
