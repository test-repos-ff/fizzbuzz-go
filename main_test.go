package main

import "testing"

func Test_fizzbuzz(t *testing.T) {
	type args struct {
		limit int
	}
	tests := []struct {
		name       string
		args       args
		wantResult string
	}{
		{name: "1to3", args: args{limit: 3}, wantResult: "1 2 Fizz"},
		{name: "1to5", args: args{limit: 5}, wantResult: "1 2 Fizz 4 Buzz"},
		{name: "1to16", args: args{limit: 16}, wantResult: "1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := fizzbuzz(tt.args.limit); gotResult != tt.wantResult {
				t.Errorf("fizzbuzz() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
