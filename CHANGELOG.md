## 2.2.3 (2022-02-17)

No changes.

## 2.2.2 (2022-02-17)

### try (1 change)

- [ci(changelog): last try](test-repos-ff/fizzbuzz-go@aaec5d2d7a006689028f03b575405672bbffd2bd)

## 2.2.1 (2022-02-17)

### try (1 change)

- [ci(changelog): last try](test-repos-ff/fizzbuzz-go@aaec5d2d7a006689028f03b575405672bbffd2bd)

### fix (1 change)

- [fix: special character for curl changelog](test-repos-ff/fizzbuzz-go@90d463a92ee66702994bbb85b85b50e0255825a3)

## 2.2.0 (2022-02-17)

### bug (1 change)

- [close #3](test-repos-ff/fizzbuzz-go@705b667a2d29eec5d62a3684995d03ab2c925325)

## 2.1.5 (2022-02-17)

### bug (1 change)

- [close #3](test-repos-ff/fizzbuzz-go@705b667a2d29eec5d62a3684995d03ab2c925325)

### fix (1 change)

- [confirm if trailer works](test-repos-ff/fizzbuzz-go@f67d82cb32b0da6d342f743472d1feea5c3a84ca)

### experimental (1 change)

- [ci: try to change type of script](test-repos-ff/fizzbuzz-go@3560d154aa50d2e23320536c9180aad0e41bdd9e)

## 2.1.4 (2022-02-17)

### feature (1 change)

- [ci: test changelog trailer](test-repos-ff/fizzbuzz-go@459979c2329f4e9abc5b53b14d2298d05e83b299)

## 2.1.3 (2022-02-17)

No changes.

## 2.1.2 (2022-02-17)

No changes.

## 1.2.0 (2022-02-17)

No changes.

## 1.1.0 (2022-02-17)

No changes.
